#' Extracts weak layer and slab characteristic for specified layer.
#'
#' Extracts weak layer and slab characteristic for specified layer. Can be specific date or range of dates.
#' @param Profiles List of profiles
#' @param LayerDateStart Singe date for weak layer or start date for range of date for weak layer
#' @param LayerDateEnd End date for range of date for weak layer
#' @param LayerLabel Optional label for weak layer
#' @param MethodSingleDate Method for identifying layer with single date. Options are previous (default), closest, and exact. See [findDATE] for details.
#' @param MethodRangeDate Method for identfying weakest layer within range of dates. Currently only option is 'ssi'. See [findWEAKEST] for details.
#' @param AddMeta Adds detailed profile meta data to each row in the output dataframe
#'
#' @return List of data frames with observations.
#' 
#' @seealso [extractHNX], [extractHST], [findWEAKEST]
#' 
#' @examples
#' 
#' ## Track a single layer
#' LayerDateStart <-  '2018-12-04'
#' LayerData <- extractLayers(SPgroup, LayerDateStart, LayerLabel = 'Dec 4')
#' LayerData$`Dec 4`$Obs
#' plot(SPgroup, SortMethod = 'unsorted')
#' points(LayerData$`Dec 4`$Obs$slab_bottom, cex = 3*LayerData$`Dec 4`$Obs$wl_gsize, pch = 20)
#' 
#' ## Track several layers with range of formation dates
#' LayerLabel <-      c('Early season pwl', 'Jan 16 SH')
#' LayerDateStart <-  c('2018-11-20',         '2019-01-14')
#' LayerDateEnd <-    c('2018-12-10',         '2019-01-17')
#' LayerData <- extractLayers(SPgroup,
#'                            LayerDateStart = LayerDateStart,
#'                            LayerDateEnd = LayerDateEnd,
#'                            LayerLabel = LayerLabel,
#'                            AddMeta = TRUE)
#' LayerData
#'
#' @export
#' 
extractLayers <- function(Profiles, 
                          LayerDateStart, 
                          LayerDateEnd = LayerDateStart,
                          LayerLabel = NA, 
                          MethodSingleDate = "previous", 
                          MethodRangeDate = "ssi",
                          AddMeta = FALSE) {
    
    ## Function to  track individual layer
    extractLayer <- function(Profiles, DateStart, DateEnd = DateStart) {
        
        ## Prep
        DateStart <- as.Date(DateStart)
        DateEnd <- as.Date(DateEnd)
        
        ## Find layer index for the target layer in each profile
        ## for a single date via findDATE or a range of dates via findWEAKEST based on ssi
        if (DateStart == DateEnd) {
            LyrIndex <- lapply(Profiles, findDATE, LayerDate = DateStart, Method = MethodSingleDate)
        } else {
            LyrIndex <- lapply(Profiles, findWEAKEST, LayerDateStart = DateStart, LayerDateEnd = DateEnd, Method = MethodRangeDate)
        }
        LyrIndex <- do.call('rbind', LyrIndex)$row
        
        ## Calculate slab properties for the layer in each profile
        LyrData <- lapply(1:length(Profiles), function(i) calcSlabProperties(Profiles[[i]], LayerIndex = LyrIndex[i]))
        LyrData <- do.call('rbind', LyrData)
        
        ## Add metadata
        ProMeta <- summary(Profiles)
        if (AddMeta) {
            LyrData <- cbind(ProMeta, LyrData)
        } else {
            LyrData <- cbind(date = ProMeta$date, LyrData)
        }
        
        ## Delete records that do not include any data (generally before burial)
        LyrData <- LyrData[!is.na(LyrData$wl_date),]
        
        ## Return
        return(LyrData)
    }
    
    ## Tracking multiple layers
    
    ## Check label
    if (is.na(LayerLabel[1])) LayerLabel <- paste(LayerDateStart, '-', LayerDateEnd)
    
    ## Create a list of results for each layer 
    Output <- lapply(1:length(LayerDateStart), function(i) return(list(Label = LayerLabel[i], 
                                                                       LayerDateStart = LayerDateStart[i], 
                                                                       LayerDateEnd = LayerDateEnd[i], 
                                                                       Obs = extractLayer(Profiles, LayerDateStart[i], LayerDateEnd[i]))))
    names(Output) <- LayerLabel
    
    ## Return
    return(Output)
}
